# Načtení stránky:

<img src="square1.png"
     alt="Základní čtverec"
     width="400px" />

# Prvních pár kliknutí:

<img src="square2.png"
     alt="Základní čtverec"
     width="400px" />

# Atd atd atd:

<img src="square3.png"
     alt="Základní čtverec"
     width="400px" />
