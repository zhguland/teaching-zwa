// initialize square element when the page has loaded
window.onload = function () {
  var mainDiv = document.getElementById("main_square");
  mainDiv.onclick = split;
  mainDiv.style["background-color"] = getRandomHexColor();
};

// return string that represents random color in hex form (e.g. #f08080)
function getRandomHexColor() {
  var randomColor = Math.floor(Math.random() * 16777215).toString(16);
  return "#" + randomColor;
}

// return new element that represents one half of the split element
function createOneHalf() {
  var element = document.createElement("div");
  element.style["background-color"] = getRandomHexColor();
  element.style["flex-grow"] = "1";
  element.onclick = split;
  return element;
}

// event handler that splits the clicked element
function split(event) {
  var clickedElement = event.target;
  var width = clickedElement.offsetWidth;
  var height = clickedElement.offsetHeight;
  if (width == 8 && height == 8) {
    return;
  }
  clickedElement.style["display"] = "flex";
  clickedElement.style["flex-direction"] = width < height ? "column" : "row";
  clickedElement.onclick = undefined;
  clickedElement.appendChild(createOneHalf());
  clickedElement.appendChild(createOneHalf());
}
