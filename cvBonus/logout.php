<?php
  session_start();
  session_destroy();
?>

<html>
  <head>
    <title>Logged out</title>
  </head>
  <body>
    <p>User logged out successfully</p>
    <a href="index.php">Home</a>
  </body>
</html>
