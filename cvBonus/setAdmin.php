<?php
  include "functions.php";

  session_start();

  if (
    !isset($_POST["email"]) ||
    !isset($_POST["value"]) ||
    !isset($_SESSION["user"]) ||
    !$_SESSION["user"]["isAdmin"]
  ) {
    http_response_code(403);
  } else {
    $email = $_POST["email"];
    $accounts = loadAccounts();
    $account = &findAccount($email, $accounts);
    if (empty($account)) {
      http_response_code(404);
    } else {
      foreach($accounts as &$account) {
        if ($account["email"] == $email) {
          $account["isAdmin"] = $_POST["value"] == 'true' ? true : false;
        }
      }
      saveAccounts($accounts);
    }
  }
?>
