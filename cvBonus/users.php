<?php
  include "functions.php";

  session_start();

  if (!isset($_SESSION["user"])) {
    header('Location: login.php');
  }
?>

<html>
  <head>
    <title>Logged out</title>
    <script>
      function setAdmin(email, value) {
        var req = new XMLHttpRequest();
        req.open("POST", '/setAdmin.php');
        var formData = new FormData();
        formData.append('email', email);
        formData.append('value', value);
        req.send(formData);
      }
    </script>
  </head>
  <body>
    <?php
      if (!$_SESSION["user"]["isAdmin"]) {
        echo '<h1> Forbidden: User ' . $_SESSION["user"]["email"] . ' not authorized to see this page !</h1>';
      } else {
        $accounts = loadAccounts();
        printHeader();
        foreach ($accounts as $account) {
          $email = $account["email"];
          if ($email == $_SESSION["user"]["email"]) {
            continue;
          }
          $isAdmin =  $account["isAdmin"];
          echo '<hr>';
          echo '<h3>email: ' . $email . '</h3>';
          if ($isAdmin) {
            echo '<button value="set as admin" onclick=\'setAdmin("'.$email.'", false)\' ">Remove admin</button>';
          } else {
            echo '<button value="set as admin" onclick=\'setAdmin("'.$email.'", true)\' ">Set admin</button>';
          }
        }
        echo '<hr>';
      }
    ?>
    <a href="index.php">Home</a>
  </body>
</html>
