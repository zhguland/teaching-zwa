let n = 0;

function printToConsole() {
  console.log("printed: " + n);
  n++;
}

function addImage() {
  const imgElement = document.createElement("img");
  imgElement.setAttribute("src", "chair2.jpg");
  document.body.appendChild(imgElement);
}

function addRemoteImage() {
  const imgElement = document.createElement("img");
  imgElement.setAttribute(
    "src",
    "https://www.beeventhire.co.uk/wp-content/uploads/2019/02/Black-Chiavari-Chair-Hire.jpg"
  );
  document.body.appendChild(imgElement);
}
