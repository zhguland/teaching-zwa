const SEARCH_DELAY_MS = 1000;
const WORDS_URL = "https://wa.toad.cz/passwords.txt";

let timeoutId = 0;

window.onload = handlePageLoad;

function handlePageLoad() {
  const inputElement = window.document.querySelector("input");
  inputElement.oninput = handleChangeOfUserInput;
}

function handleChangeOfUserInput(event) {
  const searchedWord = event.target.value;

  function createSearchRequest() {
    function showSearchResult() {
      const wordExists = doesWordExist(searchedWord, this.response);
      const messageSuffix = wordExists ? "exists !" : "not found";
      const message = `Word ${searchedWord} ${messageSuffix}`;
      showMessage(message);
    }

    function showError(error) {
      const errorText = JSON.stringify(error, null, 2);
      showMessage(`Error: ${errorText}`);
    }

    const request = new XMLHttpRequest();
    request.open("GET", WORDS_URL);
    request.onload = showSearchResult;
    request.onerror = showError;
    request.send();
  }

  showMessage("");
  clearTimeout(timeoutId);
  if (searchedWord) {
    timeoutId = setTimeout(createSearchRequest, SEARCH_DELAY_MS);
  }
}

function doesWordExist(searchedWord, responseText) {
  return responseText.trim().split("\n").includes(searchedWord);
}

function showMessage(message) {
  window.document.querySelector("p").innerHTML = message;
}
