<?php
  include "functions.php";

  session_start();

  $accounts = loadAccounts();
  $emailExists = false;
  $passwordMismatch = false;

  if (isset($_POST["email"], $_POST["password"], $_POST["repeatPassword"])) {
    $email = $_POST["email"];
    $password = $_POST["password"];
    $repeatPassword = $_POST["repeatPassword"];

    // check if the email is already registered
    foreach($accounts as $account) {
      if ($account["email"] == $email) {
        $emailExists = true;
        break;
      }
    }

    if (!$emailExists) {
      if ($password == $repeatPassword) {
        // register new account
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);
        $newAccount = array("email" => $email, "password" => $passwordHash);
        array_push($accounts, $newAccount);
        saveAccounts($accounts);
        // redirect to welcome page
        header('Location: registered.php');
      } else {
        $passwordMismatch = true;
      }
    }
  }
?>

<html>
  <head>
    <link rel="stylesheet" href="style.css" />
  </head>
  <body>
    <?php printHeader(); ?>
    <form method="post">
      <label for="email">email:</label>
      <input type="email" id="email" name="email" required />
      <?php
        if ($emailExists) {
          echo "<span class='error'>Email already used !</span>";
        }
      ?>
      <br>
      <label for="password">password:</label>
      <input type="password" id="password" name="password" required />
      <br>
      <label for="repeatPassword">repeat password:</label>
      <input type="password" id="repeatPassword" name="repeatPassword" required />
      <br>
      <?php
        if ($passwordMismatch) {
          echo "<span class='error'>Passwords do not match !</span>";
        }
      ?>
      <input type="submit" value="Register!" />
    </form>
  </body>
</html>
