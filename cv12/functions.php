<?php
  function loadAccounts() {
    if (!file_exists("accounts.json")) {
      file_put_contents("accounts.json", '[]');
    }
    return json_decode(file_get_contents("accounts.json"), true);
  }

  function saveAccounts($accounts) {
    $fileContent = json_encode($accounts, JSON_PRETTY_PRINT);
    file_put_contents("accounts.json", $fileContent);
  }

  function printHeader() {
    if (isset($_SESSION["email"])) {
      echo "<header> Logged in as " . $_SESSION["email"];
      echo ' (<a href="logout.php">Logout</a>)</header>';
    }

    // ignore directories and query string
    $file = strtok(basename($_SERVER["REQUEST_URI"]), '?');

    if ($file != 'login.php') {
      echo '<a href="login.php">Login</a>';
      echo '<br>';
    }
    if ($file != 'register.php') {
      echo '<a href="register.php">Register</a>';
      echo '<br>';
    }
    if ($file != 'index.php' && $file != '') {
      echo '<a href="index.php">Home</a>';
      echo '<br>';
    }
    echo '<br>';
  }
?>
